import pandas as pd
from statistics import mean
from numpy import linalg as LA
import numpy as np
import csv
import matplotlib.pyplot as plt

NUM_DATA = 10000
NUM_FTS = 182
NUM_TEST = 3500

data_train = np.array(pd.read_csv('Fashion_MNIST/Train_Data.csv', header=None))
label_train = np.array(pd.read_csv('Fashion_MNIST/Train_Labels.csv', header=None))
data_test = np.array(pd.read_csv('Fashion_MNIST/Test_Data.csv', header=None))
label_test = np.array(pd.read_csv('Fashion_MNIST/Test_Labels.csv', header=None))

def train(m = -1):
	global w, b, a, h, label_for_test, step_is_const, loss
	for i in range(NUM_DATA):
		if(m >= 0):
			if (i > (m * NUM_DATA / 5)) and (i < ((m+1) * NUM_DATA / 5)):
				continue

		y = -1
		if(label_train[i][0] == label_for_test):
			y = 1

		if (y * (np.dot(w, data_train[i : i+1][0]) - b)) < 1:
			w = w * (1 - 2 * a * h) - a * y * data_train[i : i+1][0]
		else:
			w = w * (1 - 2 * a * h)
			b = b + a * y
		
		# calculating loss
		loss.append(h * LA.norm(w) + max([0, 1 - y * (np.dot(w, data_train[0: 1][0]) - b)])/NUM_FTS)
	

	if step_is_const == 0:
		a = a / 1.5

label_for_test = 6 #for which label are we running the algorithm?
step_is_const = 0 #is our step decreasing or not?
loss = []
w = np.random.normal(0, 0.1, NUM_FTS) #initializng w
b = 0
a = 0.1
h = 10 ** (1)	

for k in range(50): #testing for different epocks
	train(-1)
	
plt.plot(loss)
plt.show()

