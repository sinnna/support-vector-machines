import pandas as pd
from statistics import mean
from numpy import linalg as LA
import numpy as np
import csv
from numpy import random

NUM_DATA = 10000
NUM_FTS = 182
NUM_TEST = 3500
BATCH_SIZE = 5

data_train = np.array(pd.read_csv('Fashion_MNIST/Train_Data.csv', header=None))
label_train = np.array(pd.read_csv('Fashion_MNIST/Train_Labels.csv', header=None))
data_test = np.array(pd.read_csv('Fashion_MNIST/Test_Data.csv', header=None))
label_test = np.array(pd.read_csv('Fashion_MNIST/Test_Labels.csv', header=None))

def accuracy(m = -1):
	global w, b
	correct = 0
	if(m < 0):
		for i in range(NUM_TEST):
			if((label_test[i][0] == label_for_test) and ((np.dot(w, data_test[i : i+1][0]) - b) > 1)):
				correct += 1
			if((label_test[i][0] != label_for_test) and ((np.dot(w, data_test[i : i+1][0]) - b) < -1)):
				correct += 1
		return (correct/NUM_TEST)
	
	else:
		for i in range(NUM_DATA):
			if (i < (m * NUM_DATA / 5)) or (i > ((m+1) * NUM_DATA / 5)):
				continue
			if((label_train[i][0] == label_for_test) and ((np.dot(w, data_train[i : i+1][0]) - b) > 1)):
				correct += 1
			if((label_train[i][0] != label_for_test) and ((np.dot(w, data_train[i : i+1][0]) - b) < -1)):
				correct += 1
		return (correct*5/NUM_DATA)

def train():
	global w, b, a, h, label_for_test, step_is_const, loss
	batch_list = random.choice(list(range(NUM_DATA)), BATCH_SIZE)
	for i in (batch_list):
		y = -1
		if(label_train[i][0] == label_for_test):
			y = 1

		if (y * (np.dot(w, data_train[i : i+1][0]) - b)) < 1:
			w = w * (1 - 2 * a * h) - a * y * data_train[i : i+1][0]
		else:
			w = w * (1 - 2 * a * h)
		b = b + a * y

	if step_is_const == 0:
		a = a / 2

label_for_test = 6 #for which label are we running the algorithm?
step_is_const = 0 #is our step decreasing or not?
h = 10 ** (-4)
w = np.random.normal(0, 0.1, NUM_FTS) #initializng w
b = 0
a = 0.1

for k in range(100): #testing for different epocks
	train()


#testing
acc = accuracy(-1)
print("accuracy with 5-fold cross validation is : ", acc * 100, "%")
